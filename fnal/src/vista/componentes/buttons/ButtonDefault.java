/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.componentes.buttons;

import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JButton;

/**
 *
 * @author lukas
 */
public final class ButtonDefault extends JButton{
    
    private final Color[] COLORS= {new Color(60, 154, 204), new Color(70, 178, 157),new Color(204, 60, 17)};
    private String names;
    private int datatype;

    public String getNames() {
        return names;
    }

    public void setNames(String name) {
        this.names = name;
    }

    public int getDatatype() {
        return datatype;
    }

    public void setDatatype(int datatype) {
        this.datatype = datatype;
    }
    
    public ButtonDefault(){
        setStyle();
    }
    /**
     *
     * @param type
     */
    public ButtonDefault(int type){
        setType(type);
        setStyle();
    }
    
    /**
     * @param type
     *  0-  DEFAULT
     *  1-  SUCCESS
     *  2-  DANGER
     * @param icon
     * 
     */
    public ButtonDefault(int type, int icon){
        setType(type);
        setIco(icon);
        setStyle();
    }
    
    public void setIco(int icon){
        switch(icon){
            case 0:
                setIcon(null);
                break;
            case 1:
                setIcon(new ImageIcon(getClass().getResource("/vista/icons/settings.png")));
                break;
            case 2:
                setIcon(new ImageIcon(getClass().getResource("/vista/icons/multiply.png")));
            break;
            case 3:
                setIcon(new ImageIcon(getClass().getResource("/vista/icons/conection.png")));
            break;
            case 4:
                setIcon(new ImageIcon(getClass().getResource("/vista/icons/settings_1.png")));
            break;
        }
    }
    
    public void setType(int type){
        if(type<=COLORS.length && type>=0){
            this.setBackground(COLORS[type]); 
        }
    }
    
    private void setStyle(){
        
        this.setFont(new java.awt.Font("Consolas", 0, 11)); // NOI18N
        this.setForeground(new java.awt.Color(255, 255, 255));
        this.setBorder(javax.swing.BorderFactory.createCompoundBorder(
                javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), 
                javax.swing.BorderFactory.createEmptyBorder(10, 5, 10, 5)));
        this.setContentAreaFilled(false);
        this.setFocusPainted(false);
        this.setOpaque(true);
    }
}
