/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.componentes.panels;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import vista.loockAndFeell.Colours;

/**
 *
 * @author lukas
 */
public class ScrollPanel extends JScrollPane {

    private JPanel container;

    public ScrollPanel() {
        init();

    }

    private void init() {
        container = new JPanel();
        container.setBackground(Colours.BACKGROUND);
        container.setLayout(null);
        
        container.setPreferredSize(new Dimension(300, 500));
        this.setViewportView(container);
        this.setBorder(null);
        this.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                //container.setBounds(0,0,getBounds().width, getBounds().height);
                for (Component component : container.getComponents()) {
                    ((ComponentPanel) component).resize();
                }
            }
        });
    }

    public JPanel getContainer() {
        return container;
    }

    public void setContainer(JPanel container) {
        this.container = container;
    }

    public void paint() {
        container.setPreferredSize(new Dimension(300, container.getComponentCount() * 45 + 10));
        container.repaint();
        repaint();
        validate();

    }

    public void clear() {
        container.removeAll();
    }

    public void addDb(ComponentPanel db) {
        container.add(db);
        paint();
    }

    public void clearChildStyle() {
        for (Component component : container.getComponents()) {
            ((ComponentPanel) component).setDefault();
        }
    }

}
