/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.componentes.panels;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.DefaultButtonModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import vista.componentes.buttons.ButtonDefault;
import vista.loockAndFeell.Colours;


/**
 *
 * @author lukas
 */
public class ComponentPanel  extends JPanel{
    
    private final int width= 300;
    private final int heigths = 35;
    private JLabel icon;
    private ButtonDefault conf;
    private int offset;
    private final Color DEFAUL_COLOR=new Color(50, 77, 92);
    private final Color BORDER_COLOR=new Color(70, 178, 157);
    private final Color ACTIVE=new Color(50, 127, 113);
    private final Color ERROR=new Color(204, 60, 71);
    public ComponentPanel(){
       init(null);
    }
    
    public ComponentPanel(String name) {
        init(name);
    }
    
    private void init(String icons){
        offset=70;
        if(icons!=null){
            try{
            icon=new JLabel(new ImageIcon(getClass().getResource("/vista/icons/"+icons+".png")));
            }catch(Exception e){
                System.out.println("Error al cargar el icono :"+e.getLocalizedMessage());
            }
        }
        icon.setBounds(0, 0, 50, heigths);
        icon.setOpaque(true);
        icon.setBackground(BORDER_COLOR);
        this.add(icon);
        this.setBackground(Colours.BACKGROUND);
        this.setLayout(null);
        this.setBounds(0,0,width, heigths);
        this.setPreferredSize(new Dimension(width, heigths));
        this.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, BORDER_COLOR));
    }
    public void resize(){
        getComponents()[getComponentCount()-1].setBounds(50, 0, getParent().getBounds().width-offset, 35);
        this.setBounds(getBounds().x, getBounds().y, getParent().getBounds().width-40, getBounds().height);
    }
    public void setConfButton(JButton buttond){
        offset+=20;
        buttond.setBounds(50, 10, 25, 23);
        buttond.setOpaque(true);
        buttond.setBorder(null);
        conf=(ButtonDefault) buttond;
        conf.setIco(4);
        this.add(buttond);
    }

    public void setActive(){
        icon.setBackground(BORDER_COLOR);
        this.setBackground(ACTIVE);
    }
    public void setDefault(){
        icon.setBackground(BORDER_COLOR);
        this.setBackground(Colours.BACKGROUND);
    }
    public void setError(){
        icon.setBackground(ERROR);
        this.setBackground(Colours.BACKGROUND);
    }
    public void addButton(JButton button){
        button.setBounds(100, 0, 180, 35);
        button.setOpaque(false);
        button.setBorder(null);
        this.add(button);
    }
}
