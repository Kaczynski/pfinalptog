/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.Coordinador;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import model.vo.Cool;
import vista.componentes.buttons.ButtonDefault;
import vista.componentes.combobox.CustonCombobox;
import vista.componentes.panels.PanelGrey;
import vista.loockAndFeell.Colours;

/**
 *
 * @author lukas
 */
public class ColumnOptions extends JFrame implements ActionListener {

    private int selectedIndex = 0;
    private Coordinador coordinador;
    private Object[][] comboList;

    private JLabel labeltName;
    private JLabel labeltType;
    private JLabel labeltSize;
    private JLabel labeltPatern;

    private JLabel labelName;
    private JLabel labelType;
    private JLabel labelSize;
    private CustonCombobox customCombobox;
    private PanelGrey options;

    public ColumnOptions() {
        initComponents();
    }

    private void initComponents() {
        comboList = new Object[][]{{"Tipo no soportado", 0}};

        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setSize(450, 440);
        this.setTitle("Mysql Config");

        JPanel container = new JPanel();
        container.setBackground(Colours.BACKGROUND);
        container.setLayout(null);

        options = new PanelGrey();
        options.setBounds(0, 170, 450, 200);
        container.add(options);
        labeltName = new JLabel("Nombre:");
        labeltType = new JLabel("Tipo:");
        labeltSize = new JLabel("Tamaño:");
        labeltPatern = new JLabel("Tipo de Dato:");

        labeltName.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labeltType.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labeltSize.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labeltPatern.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

        labeltName.setBounds(10, 10, 150, 30);
        labeltType.setBounds(10, 50, 150, 30);
        labeltSize.setBounds(10, 90, 150, 30);
        labeltPatern.setBounds(10, 130, 150, 30);

        labeltName.setForeground(Colours.WHITE);
        labeltType.setForeground(Colours.WHITE);
        labeltSize.setForeground(Colours.WHITE);
        labeltPatern.setForeground(Colours.WHITE);

        container.add(labeltName);
        container.add(labeltType);
        container.add(labeltSize);
        container.add(labeltPatern);

        labelName = new JLabel("");
        labelType = new JLabel("");
        labelSize = new JLabel("");

        labelName.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelType.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelSize.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        labelName.setBounds(210, 10, 190, 30);
        labelType.setBounds(210, 50, 190, 30);
        labelSize.setBounds(210, 90, 190, 30);

        labelName.setForeground(Colours.WHITE);
        labelType.setForeground(Colours.WHITE);
        labelSize.setForeground(Colours.WHITE);

        container.add(labelName);
        container.add(labelType);
        container.add(labelSize);

        customCombobox = new CustonCombobox();
        customCombobox.setPreferredSize(new Dimension(120, 30));
        customCombobox.setEditable(true);
        customCombobox.addItems(comboList);
        customCombobox.setBounds(210, 130, 190, 30);
        container.add(customCombobox);

        ButtonDefault btnCancel = new ButtonDefault(2);
        ButtonDefault btnSave = new ButtonDefault(1);
        btnCancel.setBounds(10, 370, 200, 30);
        btnSave.setBounds(220, 370, 200, 30);
        btnCancel.setText("Cancelar");
        btnSave.setText("Aceptar");
        btnCancel.setActionCommand("close");
        btnSave.setActionCommand("save");
        btnCancel.addActionListener(this);
        btnSave.addActionListener(this);

        container.add(btnSave);
        container.add(btnCancel);
        this.getContentPane().add(container);

        customCombobox.addActionListener(this);
    }

    public void setCoordinador(Coordinador miCoordinador) {
        this.coordinador = miCoordinador;
    }

    public void showCoolConfig(Cool columna) {
        labelName.setText(columna.getName());
        labelType.setText(columna.getType());
        labelSize.setText(String.valueOf(columna.getLength()));

        comboList = new Object[][]{
            //{"[NAME]"            ,[typeImput],    [typeData]}
            {"Nombres", 0, 1},
            {"Nombre Apellido", 0, 2},
            {"Correo Electronico", 0, 3},
            {"Empresa", 0, 4},
            {"Fecha", 0, 5},
            {"Pais", 0, 6},
            {"Ciudad", 0, 7},
            {"Codigo postal", 0, 8},
            {"Region", 0, 9},
            {"Numero fijo de palabras", 1, 10},
            {"Numero aleatorio de palabras", 2, 11},
            {"Rango numerico", 2, 12},
            {"Con decimales", 3, 13}
        };

        customCombobox.removeAllItems();
        customCombobox.addItems(comboList);
        if (columna.getDatacode() > 0) {
            System.out.println("se recibe: "+columna.getDatacode());
            customCombobox.setSelectedIndex(columna.getDatacode());
            int temp=0;
            for (int i = 0;i<options.getComponentCount();i++) {
                    if (options.getComponent(i) instanceof JTextField) {
                        ((JTextField) options.getComponent(i)).setText(Integer.toString(columna.getOpciones().get(temp)));
                        temp++;
                    }
                }
        }
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("save")) {
            this.save();
        }

        if (e.getActionCommand().equals("close")) {
            System.out.println("selecionado tipo:" + customCombobox.getSelectedItem());
            this.dispose();

        }
        if (e.getActionCommand().equals("comboBoxChanged")) {
            int o = customCombobox.getSelectedIndex();
            if (o != -1) {
                selectedIndex = o;
                customCombobox.setSelectedIndex(selectedIndex);
            }
            Object selected = customCombobox.getSelectedItem();
            try {
                if (selected != null) {
                    Object[] t = (Object[]) selected;
                    options.removeAll();

                    if ((int) t[1] == 2) {

                        JLabel lab1 = new JLabel("Rango:");
                        lab1.setBounds(10, 10, 150, 30);
                        lab1.setForeground(Colours.WHITE);
                        lab1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
                        options.add(lab1);

                        JTextField int1 = new JTextField("0");
                        int1.setBounds(210, 10, 80, 30);
                        options.add(int1);

                        JTextField int2 = new JTextField("10");
                        int2.setBounds(310, 10, 80, 30);
                        options.add(int2);
                        css(int1);
                        css(int2);

                    }
                    if ((int) t[1] == 3) {

                        JLabel lab1 = new JLabel("Rango:");
                        lab1.setBounds(10, 10, 150, 30);
                        lab1.setForeground(Colours.WHITE);
                        lab1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
                        options.add(lab1);

                        JLabel lab2 = new JLabel("-");
                        lab2.setBounds(260, 10, 20, 30);
                        lab2.setForeground(Colours.WHITE);
                        lab2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                        options.add(lab2);

                        JLabel lab3 = new JLabel(".");
                        lab3.setBounds(330, 20, 20, 30);
                        lab3.setForeground(Colours.WHITE);
                        lab3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                        options.add(lab3);

                        JTextField int1 = new JTextField("0");
                        int1.setBounds(210, 10, 50, 30);
                        options.add(int1);

                        JTextField int2 = new JTextField("10");
                        int2.setBounds(280, 10, 50, 30);
                        options.add(int2);

                        JTextField int3 = new JTextField("1");
                        int3.setBounds(350, 10, 50, 30);
                        options.add(int3);

                        css(int1);
                        css(int2);
                        css(int3);

                    }
                    if ((int) t[1] == 1) {
                        JLabel lab1 = new JLabel("Palabras a generar:");
                        lab1.setBounds(10, 10, 150, 30);
                        lab1.setForeground(Colours.WHITE);
                        lab1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
                        options.add(lab1);

                        JTextField int1 = new JTextField("10");
                        int1.setBounds(210, 10, 100, 30);
                        options.add(int1);
                        css(int1);
                    }
                    options.repaint();
                    repaint();
                }
            } catch (Exception ex) {
                //System.out.println(ex.getMessage());
            }

        }

    }

    private void css(JTextField int1) {
        int1.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createMatteBorder(0, 0, 2, 0, Color.yellow),
                BorderFactory.createEmptyBorder(5, 5, 5, 5)));

        int1.setBackground(Colours.BACKGROUND_LIGTH);
        int1.setForeground(Colours.WHITE);
        int1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        int1.setCaretColor(Colours.WHITE);
    }
    private void save(){
            if (selectedIndex == -1) {
                System.out.println("Puta vida es -1");
            } else {

                Cool colm = new Cool();
                colm.setActive(true);
                colm.setName(labelName.getText());
                colm.setType(labelType.getText());
                colm.setLength(Integer.parseInt(labelSize.getText()));
                colm.setDatacode(selectedIndex);
                System.out.println("se manda: "+selectedIndex);
                if (selectedIndex != -1) {
                    colm.setDatacode(selectedIndex);
                }

                for (Component c:options.getComponents()) {
                    if (c instanceof JTextField) {
                        colm.getOpciones().add(Integer.parseInt(((JTextField) c).getText()));
                    }
                }
                System.out.println(colm.getName());
                this.coordinador.setCoolConfig(colm);
            }

            //this.dispose();
    }
}
