package model;

import javax.swing.JOptionPane;

import controlador.Coordinador;
import model.dao.CoolDao;
import model.dao.DatabaseDao;
import model.dao.DatabaseListDao;
import model.vo.Cool;
import model.vo.Database;
import model.vo.DatabaseList;
import model.vo.Table;

public class Logica {

    private Coordinador miCoordinador;
    private DatabaseList databaseList;
    private String databaseSelected;
    private String tableSelected;

    public void setCoordinador(Coordinador miCoordinador) {
        this.miCoordinador = miCoordinador;

    }

    private void loadCools(String table) {
        if (table != null && !table.contains(" ") && !table.equals("")) {
            for (Database db : databaseList.getDatabases()) {
                if (db.getName().equals(databaseSelected)) {
                    for (Table tb : db.getTables()) {
                        if (tb.getName().equals(table)) {
                            tb.setCools(new CoolDao().CoolDao(databaseSelected, table));
                            break;
                        }
                    }

                }
            }
        }
    }

    private void loadTable(String database) {
        if (database != null && !database.contains(" ") && !database.equals("")) {
            for (Database db : databaseList.getDatabases()) {
                if (db.getName().equals(database)) {
                    db.setTables(new DatabaseDao().DatabaseDao(database));
                    break;
                }
            }
        }
    }

    private void loadDatabaseList() {
        databaseList = new DatabaseListDao().getDatabaseList();
    }

    public DatabaseList showDatabaseList() {
        if (databaseList == null) {
            loadDatabaseList();
        }
        return databaseList;
    }

    private Database databaseExists(String database) {
        for (Database db : databaseList.getDatabases()) {
            if (db.getName().equals(database)) {
                return db;
            }
        }
        return null;
    }

    private Table tableExists(String table) {
        if (databaseSelected != null) {
            Database db = databaseExists(databaseSelected);
            for (Table tb : db.getTables()) {
                if (tb.getName().equals(table)) {
                    return tb;
                }
            }
        }
        return null;
    }

    public Database showTableList(String database) {
        Database db = databaseExists(database);
        if (db != null) {
            if (db.getTables() == null || db.getTables().isEmpty()) {
                System.out.println("Tablas no cargadas");
                loadTable(database);
            }
            databaseSelected = database;
            return db;
        }
        JOptionPane.showMessageDialog(null, "No se encuentra la base de datos: " + database, "Error", JOptionPane.ERROR_MESSAGE);
        return null;
    }

    public Table showCoolList(String table) {

        Table tb = tableExists(table);
        if (tb != null) {
            if (tb.getCools() == null || tb.getCools().isEmpty()) {
                loadCools(table);
            }
            tableSelected = table;
            return tb;
        } else {
            return null;
        }
    }

    public Cool showCoolConfig(String cool) {
        if (databaseSelected != null) {
            Database db = databaseExists(databaseSelected);
            for (Table tb : db.getTables()) {
                if (tb.getName().equals(tableSelected)) {
                    for (Cool cl : tb.getCools()) {
                        if (cl.getName().equals(cool)) {
                            return cl;
                        }
                    }
                    JOptionPane.showMessageDialog(null, "No se encuentra la cool: " + cool, "Error", JOptionPane.ERROR_MESSAGE);
                    return null;
                }
            }
            JOptionPane.showMessageDialog(null, "No se encuentra la BD: " + databaseSelected, "Error", JOptionPane.ERROR_MESSAGE);
            return null;
        }
        JOptionPane.showMessageDialog(null, "No se ha selecionado la BD: " + databaseSelected, "Error", JOptionPane.ERROR_MESSAGE);
        return null;
    }

    public boolean setCoolConfig(Cool c) {
       Table tb = tableExists(tableSelected);
        if (tb != null) {
            for(int i = 0; i<tb.getCools().size();i++){
                if(tb.getCools().get(i).getName().equals(c.getName())){
                    tb.getCools().set(i, c);
                    return true;
                }
            }
            JOptionPane.showMessageDialog(null, "No se ha selecionado la columna: " + c.getName(), "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        JOptionPane.showMessageDialog(null, "No se ha selecionado la tabla ", "Error", JOptionPane.ERROR_MESSAGE);
        return false;
    }
}
