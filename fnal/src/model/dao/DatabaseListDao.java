/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.conexion.Conexion;
import model.vo.Database;
import model.vo.DatabaseList;

/**
 *
 * @author lukas
 */
public class DatabaseListDao {
    
    public DatabaseList getDatabaseList(){
        Conexion conex= new Conexion();
        DatabaseList databaseList = new DatabaseList();
        try {    
            Statement stmt =conex.getConnection().createStatement();
            try (ResultSet res = stmt.executeQuery("show databases")) {
                while(res.next()){
                    Database database = new Database();
                    database.setName(res.getString("Database"));
                    databaseList.addDatabase(database);
                }
            }
            conex.desconectar();
            return databaseList;
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseListDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
