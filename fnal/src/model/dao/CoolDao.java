/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.conexion.Conexion;
import model.vo.Cool;

/**
 *
 * @author lukas
 */
public class CoolDao {

    public LinkedList<Cool> CoolDao(String dbName, String tableName) {
        Conexion conex = new Conexion();
        try {
            LinkedList<Cool> tableli = new LinkedList<>();
            Statement stmt = conex.getConnection().createStatement();
            try (ResultSet res = stmt.executeQuery("SELECT COLUMN_NAME, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH FROM "
                    + " INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + tableName + "' and TABLE_SCHEMA = '" + dbName + "'")) {
                //try (ResultSet res = stmt.executeQuery("SHOW COLUMNS FROM "+tableName+" FROM "+dbName)) {
                while (res.next()) {
                    Cool cool = new Cool();
                    cool.setName(res.getString("COLUMN_NAME"));
                    cool.setLength(res.getInt("CHARACTER_MAXIMUM_LENGTH"));
                    cool.setType(res.getString("DATA_TYPE"));
                    tableli.add(cool);
                }
            }
            conex.desconectar();
            return tableli;
        } catch (SQLException ex) {
            System.out.println(DatabaseListDao.class.getName());
        }
        return null;
    }
}
