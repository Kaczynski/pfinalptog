package controlador;

import model.Logica;
import model.conexion.ConfConection;

import vista.ColumnOptions;
import vista.ConectionOptions;

import vista.VentanaPrincipal;

public class Principal {

    VentanaPrincipal ventanaPrincipal;
    ConectionOptions conectionOptions;
    ColumnOptions columnOptions;

    Logica logica;

    Coordinador coordinador;

    ConfConection mysqlconfig;
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        Principal miPrincipal = new Principal();
        miPrincipal.iniciar();
    }

    private void iniciar() {
        
        
        logica = new Logica();
        coordinador = new Coordinador();
        coordinador.setMysqlconfig(mysqlconfig);
        columnOptions = new ColumnOptions();
        mysqlconfig = new ConfConection();

        ventanaPrincipal = new VentanaPrincipal();
        conectionOptions = new ConectionOptions();

        ventanaPrincipal.setCoordinador(coordinador);
        conectionOptions.setCoordinador(coordinador);
        columnOptions.setCoordinador(coordinador);
        logica.setCoordinador(coordinador);

        /*Relaciones con la clase coordinador*/
        coordinador.setVentanaPrincipal(ventanaPrincipal);
        coordinador.setConectionOptions(conectionOptions);
        coordinador.setLogica(logica);
        coordinador.setColumnOptions(columnOptions);

        ventanaPrincipal.setVisible(true);
    }

}
