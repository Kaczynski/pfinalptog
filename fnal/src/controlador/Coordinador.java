package controlador;

import model.Logica;
import model.conexion.ConfConection;
import model.vo.Cool;
import model.vo.Database;
import model.vo.DatabaseList;
import model.vo.Table;
import vista.ColumnOptions;
import vista.ConectionOptions;
import vista.VentanaPrincipal;

public class Coordinador {

    private Logica logica;
    private VentanaPrincipal ventanaPrincipal;
    private ConectionOptions conectionOptions;
    private ColumnOptions columnOptions;

    public ColumnOptions getColumnOptions() {
        return columnOptions;
    }

    public void setColumnOptions(ColumnOptions columnOptions) {
        this.columnOptions = columnOptions;
    }
    private ConfConection mysqlconfig;

    public ConfConection getMysqlconfig() {
        return mysqlconfig;
    }

    public void setMysqlconfig(ConfConection mysqlconfig) {
        this.mysqlconfig = mysqlconfig;
    }
    
    public VentanaPrincipal getVentanaPrincipal() {
        return ventanaPrincipal;
    }

    public void setVentanaPrincipal(VentanaPrincipal miVentanaPrincipal) {
        this.ventanaPrincipal = miVentanaPrincipal;
    }

    public Logica getLogica() {
        return logica;
    }

    public void setLogica(Logica miLogica) {
        this.logica = miLogica;
    }

    public ConectionOptions getConectionOptions() {
        return conectionOptions;
    }

    public void setConectionOptions(ConectionOptions conectionOptions) {
        this.conectionOptions = conectionOptions;
    }

//////////////////////////////////////////////////////////
    public DatabaseList showDatabases() {
        return logica.showDatabaseList();
    }
    public Database showTables(String database) {
        return logica.showTableList(database);
    }
    public void showConectionOptions(){
        conectionOptions.setVisible(true); 
    }

    public Table showCools(String table) {
        return logica.showCoolList(table);
    }
    public boolean setCoolConfig(Cool cool) {
        return logica.setCoolConfig(cool);
    }
    
    public void showColumnOptions(String cool){
        Cool cl=logica.showCoolConfig(cool);
        if(cl!=null){
            columnOptions.showCoolConfig(cl);
        }
        
    }
    public void saveColumnOptions(){
        columnOptions.setVisible(true); 
    }

    public void sayHola() {
        System.out.println("Hola");
    }
    
    
    
    
}
